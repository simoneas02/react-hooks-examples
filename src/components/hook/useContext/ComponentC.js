import React, { useContext } from 'react';
import { UserContext } from '../../../context/UserContext';
import { ChannelContext } from '../../../context/ChannelContext';

const ComponentC = () => {
  const user = useContext(UserContext);
  const channel = useContext(ChannelContext);

  return (
    <div>
      <h2>Component C</h2>
      <p>
        Hello {user}, you're on {channel} channel.
      </p>
    </div>
  );
};

export default ComponentC;
