import React from 'react';
import ComponentB from './ComponentB';
import { UserContext } from '../../../context/UserContext';
import { ChannelContext } from '../../../context/ChannelContext';

const ComponentA = () => (
  <div>
    <UserContext.Provider value={'Beyonce'}>
      <ChannelContext.Provider value={'friends'}>
        Component A
        <ComponentB />
      </ChannelContext.Provider>
    </UserContext.Provider>
  </div>
);

export default ComponentA;
