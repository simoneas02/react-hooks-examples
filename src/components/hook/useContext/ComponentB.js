import React from 'react';
import ComponentC from './ComponentC';

const ComponentB = () => (
  <div>
    Component B
    <ComponentC />
  </div>
);

export default ComponentB;
