import React, { useContext } from 'react';

import { CountContext } from '../../../context/CountContext';

const CompA = () => {
  const { countState, countDispatch } = useContext(CountContext);

  return (
    <div>
      <p>Counter A {countState}</p>
      <button onClick={() => countDispatch('INCREMENT')}>Increment</button>
      <button onClick={() => countDispatch('DECREMENT')}>Decrement</button>
      <button onClick={() => countDispatch('RESET')}>Reset</button>
    </div>
  );
};

export default CompA;
