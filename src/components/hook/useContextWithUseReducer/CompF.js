import React, { useContext } from 'react';

import { CountContext } from '../../../context/CountContext';

const CompF = () => {
  const { countState, countDispatch } = useContext(CountContext);

  return (
    <div>
      <p>Counter F: {countState}</p>
      <button onClick={() => countDispatch('INCREMENT')}>Increment</button>
      <button onClick={() => countDispatch('DECREMENT')}>Decrement</button>
      <button onClick={() => countDispatch('RESET')}>Reset</button>
    </div>
  );
};

export default CompF;
