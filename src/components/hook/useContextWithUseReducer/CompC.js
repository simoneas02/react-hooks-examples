import React, { useContext } from 'react';

import CompE from './CompE';
import CompF from './CompF';

import { UserContext } from '../../../context/UserContext';
import { ChannelContext } from '../../../context/ChannelContext';

const CompC = () => {
  const user = useContext(UserContext);
  const channel = useContext(ChannelContext);

  return (
    <div>
      <h2>Component C</h2>
      <CompE />
    </div>
  );
};

export default CompC;
