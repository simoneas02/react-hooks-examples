import React, { useContext } from 'react';

import CompF from './CompF';

import { UserContext } from '../../../context/UserContext';
import { ChannelContext } from '../../../context/ChannelContext';

const CompE = () => {
  const user = useContext(UserContext);
  const channel = useContext(ChannelContext);

  return (
    <div>
      <h2>Component E</h2>
      <CompF />
    </div>
  );
};

export default CompE;
