import React, { useContext } from 'react';
import { UserContext } from '../../../context/UserContext';
import { ChannelContext } from '../../../context/ChannelContext';
import CompD from './CompD';

const CompB = () => {
  const user = useContext(UserContext);
  const channel = useContext(ChannelContext);

  return (
    <div>
      <h2>Component B</h2>
      <CompD />
    </div>
  );
};

export default CompB;
