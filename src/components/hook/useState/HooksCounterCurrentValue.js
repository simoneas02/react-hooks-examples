import React, { useState } from 'react';

const HooksCounterCurrentValue = () => {
  const initialCount = 0;
  const [count, setcount] = useState(initialCount);

  return (
    <div>
      <p>Count: {count}</p>
      <button onClick={() => setcount(count => count + 1)}>+</button>
      <button
        onClick={() =>
          setcount(prevCount => (prevCount <= 0 ? prevCount : prevCount - 1))
        }
      >
        -
      </button>
      <button onClick={() => setcount(initialCount)}>reset</button>
    </div>
  );
};

export default HooksCounterCurrentValue;
