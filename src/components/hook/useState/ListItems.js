import React, { useState } from 'react';

const ListItems = () => {
  const [items, setItems] = useState([]);

  const addItem = () =>
    setItems([
      ...items,
      { id: items.length, value: Math.floor(Math.random() * 10) + 1 },
    ]);

  return (
    <div>
      <button onClick={addItem}>Add a random number</button>
      <ul>
        {items.map(({ id, value }) => (
          <li key={id}>{value}</li>
        ))}
      </ul>
    </div>
  );
};

export default ListItems;
