import React, { useState } from 'react';

const Form = () => {
  const [name, setName] = useState({ first: '', last: '' });

  return (
    <>
      <form>
        <div>
          <label htmlFor="first">First name</label>
          <input
            id="first"
            value={name.first}
            onChange={e => setName({ ...name, first: e.target.value })}
          />
        </div>

        <div>
          <label htmlFor="last">Last name</label>
          <input
            id="last"
            value={name.last}
            onChange={e => setName({ ...name, last: e.target.value })}
          />
        </div>
      </form>

      <div>
        <p>Your first name is: {name.first}</p>
        <p>Your last name is: {name.last}</p>
        <pre>{JSON.stringify(name, null, 2)}</pre>
      </div>
    </>
  );
};

export default Form;
