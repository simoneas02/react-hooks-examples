import React, { useState } from 'react';

const HooksCounter = () => {
  const [count, setcount] = useState(0);

  return (
    <div>
      <button onClick={() => setcount(count + 1)}>Hook Count: {count}</button>
    </div>
  );
};

export default HooksCounter;
