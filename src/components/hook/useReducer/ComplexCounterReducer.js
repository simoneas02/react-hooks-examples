import React, { useReducer } from 'react';
import {
  complexCountReducer,
  complexCountInitialState,
} from '../../../reducers/complexCountReducer';

const ComplexCounterReducer = () => {
  const [count, dispatch] = useReducer(
    complexCountReducer,
    complexCountInitialState
  );

  return (
    <div>
      <div>
        <p>{count.firstCounter}</p>
        <button onClick={() => dispatch({ type: 'INCREMENT', value: 1 })}>
          Increment
        </button>
        <button onClick={() => dispatch({ type: 'DECREMENT', value: 1 })}>
          Decrement
        </button>
      </div>

      <div>
        <button onClick={() => dispatch({ type: 'INCREMENT', value: 5 })}>
          Increment by 5
        </button>
        <button onClick={() => dispatch({ type: 'DECREMENT', value: 5 })}>
          Decrement by 5
        </button>
      </div>

      <div>
        <p>{count.secondCounter}</p>
        <button onClick={() => dispatch({ type: 'INCREMENT2', value: 1 })}>
          Increment 2
        </button>
        <button onClick={() => dispatch({ type: 'DECREMENT2', value: 1 })}>
          Decrement 2
        </button>
        <button onClick={() => dispatch({ type: 'RESET' })}>Reset</button>
      </div>
    </div>
  );
};

export default ComplexCounterReducer;
