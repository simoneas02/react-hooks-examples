import React, { useReducer } from 'react';
import {
  countInitialState,
  countReducer,
} from '../../../reducers/countReducer';

const MultipleCounter = () => {
  const [count, dispatch] = useReducer(countReducer, countInitialState);
  const [countTwo, dispatchTwo] = useReducer(countReducer, countInitialState);

  return (
    <>
      <div>
        <p>Counter one: {count}</p>
        <button onClick={() => dispatch('INCREMENT')}>Increment</button>
        <button onClick={() => dispatch('DECREMENT')}>Decrement</button>
        <button onClick={() => dispatch('RESET')}>Reset</button>
      </div>

      <div>
        <p>Counter two: {countTwo}</p>
        <button onClick={() => dispatchTwo('INCREMENT')}>Increment</button>
        <button onClick={() => dispatchTwo('DECREMENT')}>Decrement</button>
        <button onClick={() => dispatchTwo('RESET')}>Reset</button>
      </div>
    </>
  );
};

export default MultipleCounter;
