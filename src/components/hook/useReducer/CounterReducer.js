import React, { useReducer } from 'react';
import {
  countReducer,
  countInitialState,
} from '../../../reducers/countReducer';

const CounterReducer = () => {
  const [count, dispatch] = useReducer(countReducer, countInitialState);
  return (
    <div>
      <p>{count}</p>
      <button onClick={() => dispatch('INCREMENT')}>Increment</button>
      <button onClick={() => dispatch('DECREMENT')}>Decrement</button>
      <button onClick={() => dispatch('RESET')}>Reset</button>
    </div>
  );
};

export default CounterReducer;
