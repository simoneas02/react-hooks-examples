import React, { useLayoutEffect, useReducer } from 'react';
import Axios from 'axios';
import { userReducer, userInitialState } from '../../../reducers/userReducer';

const UserTwo = () => {
  const [{ loading, post, error }, dispatch] = useReducer(
    userReducer,
    userInitialState
  );

  useLayoutEffect(() => {
    Axios.get(`https://jsonplaceholder.typicode.com/posts/1`)
      .then(({ data }) => {
        dispatch({ type: 'FETCH_SUCCESS', payload: data });
      })
      .catch(error => {
        dispatch({ type: 'FETCH_ERROR' });
      });
  }, []);

  return (
    <div>
      {loading ? 'Loading' : <p>{post?.title}</p>}
      {error ? error : null}
    </div>
  );
};

export default UserTwo;
