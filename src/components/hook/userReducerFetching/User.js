import React, { useState, useLayoutEffect } from 'react';
import Axios from 'axios';

const User = () => {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState('');
  const [post, setPost] = useState({});

  useLayoutEffect(() => {
    Axios.get(`https://jsonplaceholder.typicode.com/posts/1`)
      .then(({ data }) => {
        setLoading(false);
        setError('');
        setPost(data);
      })
      .catch(error => {
        setLoading(false);
        setPost({});
        setError('Somethint went wrong!');
      });
  }, []);

  return (
    <div>
      {loading ? 'Loading' : <p>{post?.title}</p>}
      {error ? error : null}
    </div>
  );
};

export default User;
