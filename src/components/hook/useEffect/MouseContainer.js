import React, { useState } from 'react';
import MousePointer from './MousePointer';

const MouseContainer = () => {
  const [display, setDisplay] = useState(true);

  return (
    <div>
      <button onClick={() => setDisplay(!display)}>Toggle display</button>
      {display && <MousePointer />}
    </div>
  );
};

export default MouseContainer;
