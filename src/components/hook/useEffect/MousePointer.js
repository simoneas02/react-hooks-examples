import React, { useState, useEffect } from 'react';

function MousePointer() {
  const [{ x, y }, setMouse] = useState({ x: 0, y: 0 });

  const logMousePosition = e => setMouse({ x: e.clientX, y: e.clientY });

  useEffect(() => {
    console.log('useEffec called');
    window.addEventListener('mousemove', logMousePosition);

    return () => window.removeEventListener('mousemove', logMousePosition);
  }, []);

  return (
    <div>
      X: {x} Y: {y}
    </div>
  );
}

export default MousePointer;
