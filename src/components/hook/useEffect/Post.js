import React, { useState, useEffect } from 'react';
import axios from 'axios';

const Post = () => {
  const [posts, setPosts] = useState([]);
  const [id, setId] = useState(1);
  const [idFromButtonClick, setIdFromButtonClick] = useState(1);

  const handleChange = e => setId(e.target.value);
  const handleClick = () => setIdFromButtonClick(id);

  useEffect(() => {
    const fetchData = async () => {
      const { data } = await axios.get(
        `https://jsonplaceholder.typicode.com/posts/${idFromButtonClick}`
      );

      setPosts(data);
    };
    console.log('fetchData');
    fetchData();
  }, [idFromButtonClick]);

  return (
    <div>
      <input type="text" value={id} onChange={handleChange} />
      <button type="button" onClick={handleClick}>
        Fetch post
      </button>
      <p>{posts?.title}</p>
    </div>
  );
};

export default Post;
