import React, { useEffect, useState } from 'react';

const UpdateTitle = () => {
  const [count, setCount] = useState(0);
  const [name, setName] = useState('');

  useEffect(() => {
    console.log('hooks mount');
    document.title = `You clicked ${count} times`;
  }, [count]);

  return (
    <div>
      <button onClick={() => setCount(count + 1)}>Click {count} times</button>
      <input value={name} onChange={e => setName(e.target.value)} />
    </div>
  );
};

export default UpdateTitle;
