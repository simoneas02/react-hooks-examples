import React, { Component } from 'react';

class ClassIntervalCounter extends Component {
  state = { count: 0 };

  componentDidMount() {
    console.log('componentDidMount');
    this.interval = setInterval(this.tick, 1000);
  }

  componentWillUnmount() {
    console.log('componentWillUnmount');
    clearInterval(this.interval);
  }

  tick = () => {
    this.setState({ count: this.state.count + 1 });
  };
  render() {
    return <h1>{this.state.count}</h1>;
  }
}

export default ClassIntervalCounter;
