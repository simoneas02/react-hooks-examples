import React, { Component } from 'react';

class ClassUpdateTitle extends Component {
  state = {
    count: 0,
    name: '',
  };

  componentDidMount() {
    console.log('componentDidMount');
    document.title = `Cliked ${this.state.count} times`;
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevState.count !== this.state.count) {
      console.log('componentDidUpdate');
      document.title = `Cliked ${this.state.count} times`;
    }
  }

  render() {
    return (
      <div>
        <button onClick={() => this.setState({ count: this.state.count + 1 })}>
          Clicked {this.state.count} times
        </button>

        <input
          value={this.state.value}
          onChange={e => this.setState({ name: e.target.value })}
        />
      </div>
    );
  }
}

export default ClassUpdateTitle;
