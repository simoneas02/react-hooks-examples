import React, { useReducer } from 'react';

// import { CountContext } from './context/CountContext';

// import { countReducer, countInitialState } from './reducers/countReducer';

import './App.css';
// import ComponentB from './components/hook/useContext/ComponentB';
// import ComponentA from './components/hook/useContext/ComponentA';
// import CounterReducer from './components/hook/useReducer/CounterReducer';
// import ComplexCounterReducer from './components/hook/useReducer/ComplexCounterReducer';
// import MultipleCounter from './components/hook/useReducer/MultipleCounter';
// import ClassCounter from './components/class/ClassCounter';
// import HooksCounter from './components/hook/useState/HooksCounter';
// import HooksCounterCurrentValue from './components/hook/useState/HooksCounterCurrentValue';
// import ClassCounterCurrentValue from './components/class/ClassCounterCurrentValue';
// import Form from './components/hook/useState/Form';
// import ListItems from './components/hook/useState/ListItems';
// import ClassUpdateTitle from './components/class/ClassUpdateTitle';
// import UpdateTitle from './components/hook/useEffect/UpdateTitle';
// import ClassMousePointer from './components/class/ClassMousePointer';
// import MousePointer from './components/hook/useEffect/MousePointer';
// import MouseContainer from './components/hook/useEffect/MouseContainer';
// import ClassIntervalCounter from './components/class/ClassIntervalCounter';
// import IntervalCounter from './components/hook/useEffect/IntervalCounter';
// import Posts from './components/hook/useEffect/Posts';
// import Post from './components/hook/useEffect/Post';
// import CompA from './components/hook/useContextWithUseReducer/CompA';
// import CompB from './components/hook/useContextWithUseReducer/CompB';
// import CompC from './components/hook/useContextWithUseReducer/CompC';
// import User from './components/hook/userReducerFetching/User';
import UserTwo from './components/hook/userReducerFetching/UserTwo';

const App = () => {
  // const [count, dispatch] = useReducer(countReducer, countInitialState);

  return (
    <div className="App">
      {/* <ClassCounter />
    <HooksCounter />
    <HooksCounterCurrentValue />
    <ClassCounterCurrentValue /> */}
      {/* <Form /> */}
      {/* <ListItems /> */}
      {/* <ClassUpdateTitle /> */}
      {/* <UpdateTitle /> */}
      {/* <ClassMousePointer /> */}
      {/* <MousePointer /> */}
      {/* <MouseContainer /> */}
      {/* <ClassIntervalCounter /> */}
      {/* <IntervalCounter /> */}
      {/* <Posts /> */}
      {/* <Post /> */}
      {/* <ComponentA /> */}
      {/* <CounterReducer /> */}
      {/* <ComplexCounterReducer /> */}
      {/* <MultipleCounter /> */}

      {/* <CountContext.Provider
        value={{ countState: count, countDispatch: dispatch }}
      >
        <p>Count: {count}</p>
        <CompA />
        <CompB />
        <CompC />
      </CountContext.Provider> */}

      {/* <User /> */}
      <UserTwo />
    </div>
  );
};

export default App;
