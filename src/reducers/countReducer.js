export const countInitialState = 0;

export const countReducer = (state, action) => {
  switch (action) {
    case 'INCREMENT':
      return state + 1;
    case 'DECREMENT':
      return state <= 0 ? state : state - 1;
    case 'RESET':
      return countInitialState;
    default:
      return state;
  }
};
