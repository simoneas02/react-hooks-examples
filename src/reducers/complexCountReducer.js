export const complexCountInitialState = {
  firstCounter: 0,
  secondCounter: 10,
};

export const complexCountReducer = (state, action) => {
  switch (action.type) {
    case 'INCREMENT':
      return { ...state, firstCounter: state.firstCounter + action.value };
    case 'DECREMENT':
      return {
        ...state,
        firstCounter:
          state.firstCounter <= 0
            ? state.firstCounter
            : state.firstCounter - action.value,
      };
    case 'INCREMENT2':
      return {
        ...state,
        secondCounter: state.secondCounter + action.value,
      };
    case 'DECREMENT2':
      return {
        ...state,
        secondCounter:
          state.secondCounter <= 0
            ? state.secondCounter
            : state.secondCounter - action.value,
      };
    case 'RESET':
      return initialState;
    default:
      return state;
  }
};
